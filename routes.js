const routes = require("next-routes")();

routes
    .add("HomePage", "/")
    .add("BlogPage", "/blog/:id", "BlogPage");

module.exports = routes;
