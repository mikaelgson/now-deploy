const express = require('express');
const next = require('next');
const routes = require('./routes');

const prod = process.env.NODE_ENV !== 'development';
const port = process.env.PORT || 80;
const app = next({ dev: !prod });
const nextRouteHandler = routes.getRequestHandler(app);

app.prepare().then(() => {
    express()
        .use(nextRouteHandler)
        .listen(port, err => {
            if (err) {
                throw err;
            }
            console.log(`> Ready on ${port}`);
        });
});
