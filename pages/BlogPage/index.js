import React from 'react';
import { withRouter } from 'next/router';
import {Link} from '../../routes';


const Index = ({ router }) => {
    return (
        <div style={{
            padding: 60
        }}>
            <h1>This is a blog with id: <strong>{router.query.id}</strong></h1>
            <Link to={'/'}><a>Go to back to start</a></Link>
        </div>
    );
};

export default withRouter(Index);
