import React from 'react';
import {Link} from '../../routes';

const Index = () => {
    return (
        <div style={{
            padding: 60
        }}>
            <h1>Welcome!</h1>
            <ul>
                <li><Link to={'/blog/1'}><a>Go to blog post with id 1</a></Link></li>
                <li><Link to={'/blog/2'}><a>Go to blog post with id 2</a></Link></li>
                <li><Link to={'/blog/3'}><a>Go to blog post with id 3</a></Link></li>
            </ul>
        </div>
    );
};

export default Index;
